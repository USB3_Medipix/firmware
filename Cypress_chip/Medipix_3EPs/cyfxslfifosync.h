/*
 ## Cypress USB 3.0 Platform header file (cyfxslfifosync.h)
 ## ===========================
 ##
 ##  Copyright Cypress Semiconductor Corporation, 2010-2011,
 ##  All Rights Reserved
 ##  UNPUBLISHED, LICENSED SOFTWARE.
 ##
 ##  CONFIDENTIAL AND PROPRIETARY INFORMATION
 ##  WHICH IS THE PROPERTY OF CYPRESS.
 ##
 ##  Use of this file is governed
 ##  by the license agreement included in the file
 ##
 ##     <install>/license/license.txt
 ##
 ##  where <install> is the Cypress software
 ##  installation root directory path.
 ##
 ## ===========================
*/

/* This file contains the constants and definitions used by the Slave FIFO application example */

#ifndef _INCLUDED_CYFXSLFIFOASYNC_H_
#define _INCLUDED_CYFXSLFIFOASYNC_H_

#include "cyu3externcstart.h"
#include "cyu3types.h"
#include "cyu3usbconst.h"
#include "cyu3dma.h"

/* 16/32 bit GPIF Configuration select */
/* Set CY_FX_SLFIFO_GPIF_16_32BIT_CONF_SELECT = 0 for 16 bit GPIF data bus.
 * Set CY_FX_SLFIFO_GPIF_16_32BIT_CONF_SELECT = 1 for 32 bit GPIF data bus.
 */
#define CY_FX_SLFIFO_GPIF_16_32BIT_CONF_SELECT (1)
 /* Select the number of address lines which intern select the NUmber of sockets used at P-Port 
 * and also the number of bulk end points with the equation.
 * Number of socket = Number of bulk endpoints = 2^(CY_FX_NUMBER_OF_ADDR_LINES-1)
 * 
 * Following is the table of parameters that varies with respect to the 
 * number of address lines
 * -------------------------------------------------------------------
 * |   CY_FX_NUMBER_OF_ADDR_LINES     |  2  |  3    |  4    |   5    |
 * |   Receive Data Socket            |  1  | 1-3   | 1-7   | 1-15   |
 * |   Send Data socket               |  3  | 5-7   | 9-15  | 17-31  |
 * |   OUT Bulk Endpoints             |  1  | 1-3   | 1-7   | 1-15   | 
 * |   IN Bulk Endpoints              |  81 | 81-83 | 81-87 | 81-8F  |
 * -------------------------------------------------------------------
 */
#define CY_FX_SLFIFO_DMA_BUF_COUNT      (2)                   /* Slave FIFO channel buffer count - This is used with MANUAL DMA channel */
#define CY_FX_SLFIFO_DMA_TX_SIZE        (0)	                  /* DMA transfer size is set to infinite */
#define CY_FX_SLFIFO_DMA_RX_SIZE        (0)	                  /* DMA transfer size is set to infinite */
#define CY_FX_SLFIFO_THREAD_STACK       (0x0400)              /* Slave FIFO application thread stack size */
#define CY_FX_SLFIFO_THREAD_PRIORITY    (8)                   /* Slave FIFO application thread priority */

/* Endpoint and socket definitions for the Slave FIFO application */

/* To change the Producer and Consumer EP enter the appropriate EP numbers for the #defines.
 * In the case of IN endpoints enter EP number along with the direction bit.
 * For eg. EP 6 IN endpoint is 0x86
 *     and EP 6 OUT endpoint is 0x06.
 * To change sockets mention the appropriate socket number in the #defines. */
 
 

/* Note: For USB 2.0 the endpoints and corresponding sockets are one-to-one mapped
         i.e. EP 1 is mapped to UIB socket 1 and EP 2 to socket 2 so on */

#define CY_FX_EP_PRODUCER_1               0x01    /* EP 1 OUT from USB */
#define CY_FX_EP_CONSUMER_1               0x81    /* EP 1 IN to USB*/

#define CY_FX_EP_PRODUCER_2               0x02   /* EP 2 OUT from USB*/
#define CY_FX_EP_CONSUMER_2               0x82    /* EP 2 IN to USB*/

#define CY_FX_EP_PRODUCER_3               0x03    /* EP 2 OUT from USB*/
#define CY_FX_EP_CONSUMER_3               0x83    /* EP 2 IN to USB*/


//#define CY_FX_PRODUCER_USB_SOCKET    CY_U3P_UIB_SOCKET_PROD_1    /* USB Socket 1 is producer */
//#define CY_FX_CONSUMER_USB_SOCKET    CY_U3P_UIB_SOCKET_CONS_1    /* USB Socket 1 is consumer */
#define CY_FX_PRODUCER_USB_SOCKET_BASE CY_U3P_UIB_SOCKET_PROD_0
#define CY_FX_CONSUMER_USB_SOCKET_BASE CY_U3P_UIB_SOCKET_CONS_0

/* Used with FX3 Silicon. */


#define CY_FX_CONSUMER_PPORT_SOCKET_EP1    CY_U3P_PIB_SOCKET_0    /* P-port Socket 0 is consumer */
#define CY_FX_PRODUCER_PPORT_SOCKET_EP1    CY_U3P_PIB_SOCKET_1	  /* P-port Socket 1 is producer */

#define CY_FX_PRODUCER_PPORT_SOCKET_EP2    CY_U3P_PIB_SOCKET_2    /* P-port Socket 2 is producer */

#define CY_FX_PRODUCER_PPORT_SOCKET_EP3    CY_U3P_PIB_SOCKET_3    /* P-port Socket 3 is producer */


/* Length of this descriptor and all sub descriptors */
#define CY_U3P_HS_LENGTH_DESCRIPTOR 0x3c,0x00
#define CY_U3P_FS_LENGTH_DESCRIPTOR 0x3c,0x00 /* 2e*/
#define CY_U3P_SS_LENGTH_DESCRIPTOR 0x60,0x00 /* 46*/

/* Number of end points */  //doesn't include the EP0
#define CY_U3P_NUMBER_OF_ENDPOINTS 0x06

/* Producer socket used by the U-Port */
#define CY_FX_PRODUCER_1_USB_SOCKET     CY_FX_PRODUCER_USB_SOCKET_BASE + CY_FX_EP_PRODUCER_1     /* USB Socket 1 is producer */

/* Consumer socket used by the U-Port */
#define CY_FX_CONSUMER_1_USB_SOCKET     CY_FX_CONSUMER_USB_SOCKET_BASE + (CY_FX_EP_CONSUMER_1 & 0xF)    /* USB Socket 1 is consumer */
#define CY_FX_CONSUMER_2_USB_SOCKET     CY_FX_CONSUMER_USB_SOCKET_BASE + (CY_FX_EP_CONSUMER_2 & 0xF)    /* USB Socket 2 is consumer */
#define CY_FX_CONSUMER_3_USB_SOCKET     CY_FX_CONSUMER_USB_SOCKET_BASE + (CY_FX_EP_CONSUMER_3 & 0xF)    /* USB Socket 3 is consumer */



/* Producer socket used by the P-Port */
#define CY_FX_PRODUCER_1_PPORT_SOCKET     CY_FX_PRODUCER_PPORT_SOCKET_EP1   /* P-port Socket 1 is producer */
#define CY_FX_PRODUCER_2_PPORT_SOCKET     CY_FX_PRODUCER_PPORT_SOCKET_EP2   /* P-port Socket 2 is producer */
#define CY_FX_PRODUCER_3_PPORT_SOCKET     CY_FX_PRODUCER_PPORT_SOCKET_EP3   /* P-port Socket 3 is producer */


/* Consumer socket used by the P-Port */
#define CY_FX_CONSUMER_1_PPORT_SOCKET     CY_FX_CONSUMER_PPORT_SOCKET_EP1   /* P-port Socket 1 is CONSUMER */

/* Extern definitions for the USB Descriptors */
extern const uint8_t CyFxUSB20DeviceDscr[];
extern const uint8_t CyFxUSB30DeviceDscr[];
extern const uint8_t CyFxUSBDeviceQualDscr[];
extern const uint8_t CyFxUSBFSConfigDscr[];
extern const uint8_t CyFxUSBHSConfigDscr[];
extern const uint8_t CyFxUSBBOSDscr[];
extern const uint8_t CyFxUSBSSConfigDscr[];
extern const uint8_t CyFxUSBStringLangIDDscr[];
extern const uint8_t CyFxUSBManufactureDscr[];
extern const uint8_t CyFxUSBProductDscr[];

#include "cyu3externcend.h"

#endif /* _INCLUDED_CYFXSLFIFOASYNC_H_ */

/*[]*/
